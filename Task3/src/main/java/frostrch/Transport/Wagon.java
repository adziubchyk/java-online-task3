package frostrch.Transport;

import java.util.Collections;
import java.util.Comparator;

public class Wagon {

    protected int number;
    protected int seats;
    private int passengers=0;
    private double carriage=0;
    protected int comfortlevel;

    Wagon(){
        number=0;
        seats=0;
        comfortlevel=0;
    }
    Wagon(int number, int seats, int comfortlevel){
        this.number=number;
        this.seats=seats;
        this.comfortlevel=comfortlevel;
    }




    public void addPassenger(int amount){
        if(passengers+amount<=seats){
            passengers+=amount;
        }else System.out.println("not enought seats chose another wagon");
    }
    public void addCarriage(double weight){
        carriage+=weight;
    }
    int getFreeSeats(){
        return seats-passengers;
    }
    int getPassengers(){
        return passengers;
    }
    double getCarriage(){
        return carriage;
    }


}
