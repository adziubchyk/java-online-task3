package frostrch.Transport;

import java.util.Collections;
import java.util.Comparator;

public class PassangerTrain extends Train {
    private String name;
    private int passangerCount=0;
    private double carriaheCount=0;

    PassangerTrain(String name){
        this.name=name;
    }

    void updateInfo(){
        passangerCount=0;
        carriaheCount=0;
        for(Wagon wg:depot){
            passangerCount+=wg.getPassengers();
            carriaheCount+=wg.getCarriage();
        }
    }


    void sortWagons(){
        Collections.sort(depot, new Comparator<Wagon>() {
            @Override
            public int compare(Wagon obj1, Wagon obj2) {
                return obj1.comfortlevel - obj2.comfortlevel;
            }
        });
    }
    void searchWagons(int a, int b){
        for(Wagon wg:depot){
            if (wg.getPassengers()>=a&&wg.getPassengers()<=b)
                System.out.println(wg.number);
        }
    }
    int getPassangerCount(){
        return passangerCount;
    }
    double getCarriaheCount(){
        return carriaheCount;
    }

}
