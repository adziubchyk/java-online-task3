package frostrch.Transport;

public class Main {

    public enum Menu {

        STATS,
        SORTING,
        SEARCHING,
        EXITING
    }

    public static void main(String []args){
        PassangerTrain polissya = new PassangerTrain("806L");
        polissya.addWagon(new Berth(1));
        polissya.addWagon(new Berth(2));
        polissya.addWagon(new Berth(3));
        polissya.addWagon(new Compt(4));
        polissya.addWagon(new Compt(5));
        polissya.addWagon(new Compt(6));
        polissya.addWagon(new DeLuxe(7));
        polissya.addWagon(new DeLuxe(8));
        polissya.addWagon(new Berth(9));
        polissya.addWagon(new Berth(10));
        polissya.addWagon(new Berth(11));


        polissya.updateInfo();
        System.out.println(polissya.getPassangerCount()+" "+polissya.getCarriaheCount());
        polissya.sortWagons();
        polissya.searchWagons(12,38);


    }


}
