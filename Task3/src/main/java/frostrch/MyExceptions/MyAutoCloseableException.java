package frostrch.MyExceptions;

public class MyAutoCloseableException implements AutoCloseable {


    public void simpleWork(){
        System.out.println("some simple work");
    }

    public void close() throws Exception {
        System.out.println("MyAutoCloseableException");
        throw new MySimpleException("MySimpleException");
    }
}