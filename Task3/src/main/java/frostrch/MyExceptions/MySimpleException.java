package frostrch.MyExceptions;

public class MySimpleException extends Exception {
    public MySimpleException() {
    }

    public MySimpleException(String message) {
        super(message);
    }

    public MySimpleException(String message, Throwable cause) {
        super(message, cause);
    }

    public MySimpleException(Throwable cause) {
        super(cause);
    }
}
