package frostrch.MyExceptions;

public class Main {

    public static void main(String[] args) {

        try (MyAutoCloseableException test = new MyAutoCloseableException()){
            test.simpleWork();
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}
